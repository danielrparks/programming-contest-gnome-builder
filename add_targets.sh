#!/usr/bin/bash

cd src

recent="$(ls -Atw1 *.cpp | head -n1)"
if [[ "$recent" == "" ]]; then
	echo "nop"
	exit
fi
recent_noext="$(basename "$recent")"
targetstring="executable('$recent_noext', '$recent', install: true)"
buildfile="meson.build"

if grep -Fxq "$targetstring" "$buildfile"; then
	# already exists so don't add it
	echo "nop"
else
	echo "$targetstring" >> "$buildfile"
	echo "$recent" # for the build system to print out
fi
