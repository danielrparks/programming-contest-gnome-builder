namespace unistd {
	#include <unistd.h>
}
#include "config.hpp"

void cd() {
	unistd::chdir(WORKING_DIRECTORY);
}
