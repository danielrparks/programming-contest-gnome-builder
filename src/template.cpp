#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <fstream>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <cmath>
#include <utility>
#include <tuple>
#include <deque>
#include <queue>
#include <algorithm>
#include <iomanip>
#include <regex>

#include "cd.hpp"

int main() {
	cd();
	return 0;
}
