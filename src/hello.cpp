#include <iostream>
#include <string>
#include <fstream>
#include "cd.hpp"

int main(int argv, char** argc) {
	cd();
  std::string hello;
  hello = "Hello ";
  if (argv > 1) {
    hello += argc[1];
  }
  else {
    hello += "world!";
  }
  std::cout << hello << '\n';
  std::ofstream log;
  log.open("hello.log");
  log << hello << '\n';
  return 0;
}
