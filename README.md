# Programming contest setup for gnome-builder
This is my attempt to make Builder as programming-contest-friendly as possible.

To use this project, you need to change the username in `.buildconfig` to match yours.

To add a run configuration for a file, simply create and edit in `src/`.
Then rebuild the project. A simple configuration will automatically be added.
(You may need to build it again.)

I have also included a header, `cd.hpp`, that you can include.
Then you just need to run `cd()` at the beginning of your main function,
and your program will automatically change directory to the 'dat' directory.
